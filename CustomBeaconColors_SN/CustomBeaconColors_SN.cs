﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace CustomBeaconColors_SN
{
    public class ColorData
    {
        public byte r { get; set; }
        public byte g { get; set; }
        public byte b { get; set; }
        public byte a { get; set; }
    }

    public class Config
    {
        public ColorData[] colors { get; set; }
    }

    /**
     * Allows custom beacon colors, configurable via JSON
     */
    [BepInPlugin(myGUID, pluginName, versionString)]
    public class CustomBeaconColors : BaseUnityPlugin
    {
        private const string myGUID = "com.rrowe404.custombeaconcolorssn";
        private const string pluginName = "Custom Beacon Colors SN";
        private const string versionString = "1.0.0";

        private static readonly Harmony harmony = new Harmony(myGUID);

        public static ManualLogSource logger;

        public static Color[] colorOverrides = new Color[5]
        {
            new Color32(255, 73, 200, byte.MaxValue),
            new Color32(164, 73, 255, byte.MaxValue),
            new Color32(255, 255, 73, byte.MaxValue),
            new Color32(255, 73, 73, byte.MaxValue),
            new Color32(255, 164, 73, byte.MaxValue)
        };

        private void Awake()
        {
            harmony.PatchAll();
            Logger.LogInfo(pluginName + " " + versionString + " " + "loaded.");
            logger = Logger;

            AssignColors();
        }

        private void AssignColors()
        {
            string colorPath = Path.Combine(Environment.CurrentDirectory, "Bepinex", "plugins", Assembly.GetCallingAssembly().GetName().Name, "colors.json");

            if (File.Exists(colorPath))
            {
                try
                {
                    Config config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(colorPath));
                    colorOverrides = config.colors.Take(5).Select(color => (Color) new Color32(color.r, color.g, color.b, color.a)).ToArray();
                } catch
                {
                    logger.LogError("Error reading override file. Is it well formatted?");
                }
            } else
            {
                Logger.LogInfo("No color override file found. Using default overrides...");
            }

            FieldInfo colorOptions = typeof(PingManager).GetField("colorOptions", BindingFlags.Public | BindingFlags.Static);
            colorOptions.SetValue(null, colorOverrides);
        }
    }
}

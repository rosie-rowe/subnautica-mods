﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;

namespace DatabankScrollFix_SN
{
    [BepInPlugin(myGUID, pluginName, versionString)]
    public class DatabankScrollFix_SN : BaseUnityPlugin
    {
        private const string myGUID = "com.rrowe404.databankscrollfixsn";
        private const string pluginName = "Databank Scroll Fix SN";
        private const string versionString = "1.0.0";

        private static readonly Harmony harmony = new Harmony(myGUID);

        public static ManualLogSource logger;

        private void Awake()
        {
            harmony.PatchAll();
            Logger.LogInfo(pluginName + " " + versionString + " " + "loaded.");
            logger = Logger;
        }

        [HarmonyPatch(typeof(uGUI_EncyclopediaTab))]
        public static class uGUI_EncyclopediaTab_Patch
        {
            [HarmonyPatch(nameof(uGUI_EncyclopediaTab.DisplayEntry))]
            [HarmonyPostfix]
            public static void DisplayEntryPostfix(uGUI_EncyclopediaTab __instance)
            {
                __instance.contentScrollRect.verticalNormalizedPosition = 1;
            }
        }
    }
}

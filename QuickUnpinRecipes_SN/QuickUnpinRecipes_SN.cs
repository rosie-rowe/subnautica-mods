﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using UnityEngine.EventSystems;

namespace QuickUnpinRecipes_SN
{
    /**
     * Allows quick unpinning of recipes by clicking when in crafting menu or any PDA tab except the blueprint tab
     */
    [BepInPlugin(myGUID, pluginName, versionString)]
    public class QuickUnpinRecipes_SN : BaseUnityPlugin
    {
        private const string myGUID = "com.rrowe404.quickunpinrecipessn";
        private const string pluginName = "Quick Unpin Recipes SN";
        private const string versionString = "1.0.0";

        private static readonly Harmony harmony = new Harmony(myGUID);

        public static ManualLogSource logger;

        private void Awake()
        {
            harmony.PatchAll();
            Logger.LogInfo(pluginName + " " + versionString + " " + "loaded.");
            logger = Logger;
        }

        [HarmonyPatch(typeof(uGUI_RecipeEntry))]
        public static class uGUI_RecipeEntry_Patch
        {
            private static bool isDragLocked = false;

            /**
             * Enables quick unpin
             */
            [HarmonyPatch(nameof(uGUI_RecipeEntry.OnPointerClick))]
            [HarmonyPrefix]
            public static void OnPointerClickPrefix(uGUI_RecipeEntry __instance, PointerEventData eventData)
            {
                bool isRightClick = eventData.button == PointerEventData.InputButton.Right;

                /**
                 * Without <seealso cref="isDragLocked"/> if you right click while dragging a recipe, the game gets confused,
                 * and ends up sticking the pin to your cursor the next time you try to pin a recipe, preventing recipes from being pinned.
                 * It's easier to prevent this from happening than to try to fix it, and imo a better user experience.
                 * I often accidentally right click, and wouldn't want that to unpin a recipe I am just trying to move.
                 * See <seealso cref="OnBeginDragPostfix"/> <seealso cref="OnEndDragPostfix"/>
                 * Note that the vanilla game does have some strange behavior if you right click while dragging --
                 * it sometimes unpins anyway, sometimes pings, and sometimes returns to starting position, and sometimes does nothing.
                 * This behavior is maintained.
                 */
               bool shouldIntercept = isRightClick && !isDragLocked;

                if (shouldIntercept)
                {
                    __instance.manager.pingOnClick = false;
                    PinManager.TogglePin(__instance.techType);
                }
            }

            [HarmonyPatch(nameof(uGUI_RecipeEntry.OnBeginDrag))]
            [HarmonyPostfix]
            public static void OnBeginDragPostfix()
            {
                isDragLocked = true;
            }

            [HarmonyPatch(nameof(uGUI_RecipeEntry.OnEndDrag))]
            [HarmonyPostfix]
            public static void OnEndDragPostfix()
            {
                isDragLocked = false;
            }
        }
    }

}
